// product.dto.ts

export class CreateProductDto {
  name: string;

  description: string;

  category: string;

  imageURL: string;

  price: number;

  currency: string;

  quantity: number;
}
