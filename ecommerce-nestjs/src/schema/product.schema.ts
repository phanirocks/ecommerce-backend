import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
@Schema()
export class Product {
  @Prop()
  name: string;
  @Prop()
  description: string;
  @Prop()
  category: string;
  @Prop()
  imageURL: string;
  @Prop()
  price: number;
  @Prop()
  currency: string;
  @Prop()
  quantity: number;
}
export const ProductSchema = SchemaFactory.createForClass(Product);
