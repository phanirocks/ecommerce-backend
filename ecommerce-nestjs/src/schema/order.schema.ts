import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Types } from 'mongoose';

@Schema()
export class Order {
  @Prop()
  customerId: Types.ObjectId;
  @Prop()
  orderDate: string;
  @Prop()
  paymentMethod: string;
  @Prop()
  orderItems: OrderItem[];
  @Prop()
  status: string;
  @Prop()
  totalPrice: number;
  @Prop()
  currency: string;
  @Prop()
  shippingCompany: string;
  @Prop()
  shippingTrackingNumber: string;
  @Prop()
  email: string;
  @Prop()
  address: string;
}

export interface OrderItem {
  productId: Types.ObjectId,
  quantity: number
}
export const OrderSchema = SchemaFactory.createForClass(Order);
