import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductModule } from 'src/products/products.module';
import { ProductService } from 'src/products/products.service';
import { Order, OrderSchema } from 'src/schema/order.schema';
import { OrdersController } from './orders.controller';
import { OrdersService } from './orders.service';



@Module({
  imports: [
    MongooseModule.forFeature([{ name: Order.name, schema: OrderSchema }]),
    ProductModule
  ],
  controllers: [OrdersController],
  providers: [OrdersService],
})
export class OrdersModule {}
