import { Controller, Post, Body, NotFoundException, Get, Param, Put } from '@nestjs/common';
import { Types } from 'mongoose';
import { Order } from 'src/schema/order.schema';
import { UpdateOrderDto } from './dto/order.dto';
import { OrdersService } from './orders.service';

@Controller('orders')
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) {}

  @Post()
  async createOrder(@Body() orderData: any) {
    const createdOrder = await this.ordersService.createOrder(orderData);
    return createdOrder;
  }

  @Get(':id')
  async getOrderById(@Param('id') id: string): Promise<Order> {
    return this.ordersService.findOrderById(new Types.ObjectId(id));
  }

  @Put(':id')
  async updateOrder(
    @Param('id') id: string,
    @Body() updateProductDto: UpdateOrderDto,
  ): Promise<Order> {
    return this.ordersService.updateOrder(new Types.ObjectId(id), updateProductDto);
  }
}
