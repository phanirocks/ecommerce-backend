// product.dto.ts

import { OrderItem } from "src/schema/order.schema";

export class CreateOrderDto {

  products: OrderItem[];
  date: string;
  totalPrice: number;
  currency: string;
  email: string;
  address: string;

}

export class UpdateOrderDto {
  products: OrderItem[];
  date: string;
  totalPrice: number;
  currency: string;
  email: string;
  address: string;
  paymentMethod: string;
  status: string;
  shippingCompany: string;
  shippingTrackingNumber: string;
}