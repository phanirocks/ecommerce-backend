import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { ProductService } from 'src/products/products.service';
import { Order } from 'src/schema/order.schema';
import { CreateOrderDto, UpdateOrderDto } from './dto/order.dto';

@Injectable()
export class OrdersService {
  constructor(private readonly productsService: ProductService,  @InjectModel(Order.name) private readonly orderModel: Model<Order>,) {}

  async createOrder(orderData: CreateOrderDto) {
    // Assuming orderData contains an array of products with product IDs and quantities
    const products = orderData.products;

    // Loop through each product in the order
    for (const product of products) {
      const { productId, quantity } = product;

      // Find the product by its ID
      const foundProduct = await this.productsService.findProductById(productId);

      if (!foundProduct) {
        throw new NotFoundException(`Product with ID ${productId} not found`);
      }

      if (foundProduct.quantity < quantity) {
        throw new Error(`Insufficient quantity for product ${productId}`);
      }

      // Reduce the product quantity by the ordered quantity
      foundProduct.quantity -= quantity;

      // Update the product's quantity in the database
      await this.productsService.updateProduct(productId, foundProduct);
    }

    const order = {  
        customerId: new Types.ObjectId(),
        orderDate: new Date().toDateString(),
        paymentMethod: "TBD",
        orderItems: orderData.products,
        status: "Ordered",
        totalPrice: orderData.totalPrice,
        currency: orderData.currency,
        email: orderData.email,
        address: orderData.address,
        shippingCompany: "Not Allocated",
        shippingTrackingNumber: "Not Allocated"
    }

    const createdOrder = new this.orderModel(order);
    return createdOrder.save();

  }

 async findOrderById(id: Types.ObjectId): Promise<Order> {
    return this.orderModel.findById(id).exec();
  }


  async updateOrder(
    id: Types.ObjectId,
    updateOrderDto: UpdateOrderDto,
  ): Promise<Order> {
    return this.orderModel
      .findByIdAndUpdate(id, updateOrderDto, { new: true })
      .exec();
  }

} 
