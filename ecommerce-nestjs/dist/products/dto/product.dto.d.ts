export declare class CreateProductDto {
    name: string;
    description: string;
    category: string;
    imageURL: string;
    price: number;
    currency: string;
    quantity: number;
}
