import { Model, Types } from 'mongoose';
import { Product } from '../schema/product.schema';
import { CreateProductDto } from './dto/product.dto';
export declare class ProductService {
    private readonly productModel;
    constructor(productModel: Model<Product>);
    createProduct(createProductDto: CreateProductDto): Promise<Product>;
    findProducts(category?: string, searchTerm?: string): Promise<Product[]>;
    findProductById(id: Types.ObjectId): Promise<Product>;
    updateProduct(id: Types.ObjectId, updateProductDto: CreateProductDto): Promise<Product>;
    deleteProduct(id: string): Promise<Product>;
}
