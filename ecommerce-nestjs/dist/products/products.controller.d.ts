import { Product } from '../schema/product.schema';
import { CreateProductDto } from './dto/product.dto';
import { ProductService } from './products.service';
export declare class ProductController {
    private readonly productService;
    constructor(productService: ProductService);
    createProduct(createProductDto: CreateProductDto): Promise<Product>;
    getAllProducts(category: string, searchTerm: string): Promise<Product[]>;
    getProductById(id: string): Promise<Product>;
    updateProduct(id: string, updateProductDto: CreateProductDto): Promise<Product>;
    deleteProduct(id: string): Promise<Product>;
}
